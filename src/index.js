/**
 * Author: Shabalin Pavel
 * Email: aisamiery@gmail.com
 */
import $ from 'jquery';
import Vue from 'vue';
import Client from "./component/Client.vue";

try {
    const appInit = () => {
        new Vue({
            components: {
                Client,
            },
            render: (h) => {
                return h(Client);
            },
        }).$mount('#application');
    };

    $(document).ready(appInit);
} catch (e) {

}
